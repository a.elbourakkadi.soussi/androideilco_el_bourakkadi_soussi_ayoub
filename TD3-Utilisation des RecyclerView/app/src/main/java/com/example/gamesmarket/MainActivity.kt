package com.example.gamesmarket

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.recyclerviewtest.JeuVideo
import com.example.recyclerviewtest.MyVideosGamesAdapter
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)



        val jeuxVideos = ArrayList<JeuVideo>()
        jeuxVideos.add(JeuVideo("Call of Duty Mobile", 50,""))
        jeuxVideos.add(JeuVideo("Fortnite", 40,""))
        jeuxVideos.add(JeuVideo("PupG mobile", 100,""))
        jeuxVideos.add(JeuVideo("Minecraft", 100,""))





        //myRecyclerView.layoutManager = GridLayoutManager(this,2)
        //myRecyclerView.layoutManager = LinearLayoutManager(this)
        myRecyclerView.layoutManager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL ,false)
        myRecyclerView.adapter = MyVideosGamesAdapter(jeuxVideos)

    }
}
