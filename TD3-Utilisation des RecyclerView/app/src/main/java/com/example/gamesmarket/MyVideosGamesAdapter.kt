package com.example.recyclerviewtest

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.view.menu.ActionMenuItemView
import androidx.recyclerview.widget.RecyclerView
import com.example.gamesmarket.R
import kotlinx.android.synthetic.main.jeuvideo_layout.view.*
import java.text.FieldPosition


class MyVideosGamesAdapter(val jeulist: ArrayList<JeuVideo>) : RecyclerView.Adapter<MyVideosGamesAdapter.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.jeuvideo_layout, parent, false)
        return ViewHolder(v)
    }

    override fun getItemCount(): Int {
        return jeulist.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val jeu: JeuVideo = jeulist[position]
        val item = jeulist[position]
        holder.textViewName.text = jeu.name
        holder.textViewPrice.text = jeu.price.toString() + " $"
        holder.qualityImage.setImageResource(when (item.name) {
            "Call of Duty Mobile" -> R.drawable.jeu1
            "Fortnite" -> R.drawable.jeu2
            "PupG mobile"-> R.drawable.jeu3
            "Minecraft" -> R.drawable.jeu4
            else -> R.drawable.jeupic
        })

    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {


        val textViewName: TextView = itemView.findViewById(R.id.textViewName)
        val textViewPrice: TextView = itemView.findViewById(R.id.textViewPrice)
        val qualityImage: ImageView = itemView.findViewById(R.id.quality_image)

    }
}