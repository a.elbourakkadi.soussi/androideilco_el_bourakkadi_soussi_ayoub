package com.example.myapplication

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import java.util.*
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }

    fun calculer(view: View) {

        val add = "+"
        val sous = "-"
        val mul = "*"
        val div = "/"
        val operat = oper.text.trim().toString()

        if (operat == add)
        {
            result.text = "Resultat :"+ (num1.text.toString().toInt() + num2.text.toString().toInt())
        }
        else if (operat == sous)
        {
            result.text = "Resultat :"+ (num1.text.toString().toInt() - num2.text.toString().toInt())

        }
        else if (operat == mul)
        {
            result.text = "Resultat :"+ (num1.text.toString().toInt() * num2.text.toString().toInt())

        }
        else if (operat == div)
        {
            result.text = "Resultat :"+ (num1.text.toString().toInt() / num2.text.toString().toInt())

        }
        else
        {
            result.text = "Resultat : operateur incorrect (+, -, *, /) "
        }


    }

}
