package com.example.newslist

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import kotlinx.android.synthetic.main.activity_details.*

class ActivityDetails : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_details)

        detailstext.text="no details found for the informations in the list"

        val okButton: Button = findViewById(R.id.ok_button);
        okButton.setOnClickListener{ok()};
    }
    private fun ok()
    {
        val intent = Intent(this, LoginActivity::class.java)
        startActivity(intent)

    }

}
