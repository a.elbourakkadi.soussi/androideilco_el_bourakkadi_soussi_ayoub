package com.example.newslist

import android.app.Activity
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_login.*
import kotlinx.android.synthetic.main.activity_news.*

class ActivityNews : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_news)

        // Get data from intet
        val login = intent.getStringExtra("Login")
        val pass = intent.getStringExtra("Password")

        resulttext.text= "Hello"+"\nyour Login is : "+login+"\nyour password is : "+pass+" "

        val detailsButton: Button = findViewById(R.id.details_button)
        detailsButton.setOnClickListener{details()}

        val logoutButton: Button = findViewById(R.id.logout_button)
        logoutButton.setOnClickListener { logout() }

    }
    private fun details()
    {
        val intent = Intent(this, ActivityDetails::class.java)

        startActivity(intent);
        //Toast.makeText(applicationContext,"Connexion réussie", Toast.LENGTH_SHORT).show()
    }
    private fun logout()
    {
        val intent = Intent(this, LoginActivity::class.java)
        startActivity(intent)

    }
}
