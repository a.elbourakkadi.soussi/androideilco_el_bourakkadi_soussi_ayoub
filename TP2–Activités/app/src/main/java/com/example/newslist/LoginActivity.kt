package com.example.newslist

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import  android.widget.Button
import kotlinx.android.synthetic.main.activity_login.*
import  android.content.Intent
import android.widget.EditText
import android.widget.Toast

class LoginActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login);

        val loginButton: Button = findViewById(R.id.login_button);
        loginButton.setOnClickListener{news()};

    }

    private fun news()
    {
     //   val login="admin"
        val login =  logintext.text.trim().toString()
        val pass = passwordtext.text.trim().toString()


        if(login.equals("admin",true) && pass.equals("admin",true))
        {
            val intent = Intent(this, ActivityNews::class.java)
            intent.putExtra("Login", login)
            intent.putExtra("Password", pass)
            startActivity(intent);
            Toast.makeText(applicationContext,"Connexion réussie",Toast.LENGTH_SHORT).show()

        }
        else
        {
            Toast.makeText(applicationContext,"Login ou Password incorrect",Toast.LENGTH_SHORT).show()
        }
    }

}
